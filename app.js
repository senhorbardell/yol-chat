var app = require('express')(),
    server = require('http').Server(app),
    io = require('socket.io').listen(server);

var redis, subscriber;

process.on('exit', function() {
    redisStop();
});

server.listen(process.env.PORT || 8080, function() {
    var addr = this.address();

    console.log('State server is run on %s:%d', addr.address, addr.port);
});

//TODO Just to test in browser -> Has to be removed on production
app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(client) {
    redisInit();

    console.log('Debug: Client is connected');

    client.once('auth', function(token) {
        client.join(token);

        console.log('Debug: ' + token + ' is authed');

        redis.get(token, function(err, response) {
            if(response != null) {
                console.log('Debug: ' + token + ' has accumulated/offline state');

                // redis.del(token);
            }
            console.log(response);

            var state = err != null || response == null ? {
                'new': {
                    'chats': [],
                    'posts': [],
                    'comments': []
                }
            } : {
                'new': JSON.parse(response)['new']
            };

            io.to(token).emit('authed', state);

            console.log('Debug: ' + token + ' is sent state');
        });

        subscriber.on('pmessage', function(pattern, channel, message) {
                console.log('Debug: ' + channel + ' had a new event');

                io.to(channel).emit('state', message);

                console.log('Debug: ' + channel + ' is sent state');
        });

        subscriber.psubscribe('*');

        client.on('reconnect', function() {
            console.log('Debug: ' + token + ' is reconnected');
        });

        client.on('disconnect', function() {
            console.log('Debug: ' + token + ' is disconnected');
        });
    });
});

function redisInit() {
    redisStop();

    if(typeof process.env.REDISCLOUD_URL != 'undefined') {
        var conn = require('url').parse(process.env.REDISCLOUD_URL);

        redis = require('redis').createClient(conn.port, conn.hostname, {auth_pass: conn.auth.split(':')[1]});
        subscriber = require('redis').createClient(conn.port, conn.hostname, {auth_pass: conn.auth.split(':')[1]});

        console.log('Remote redis connection is inited');
    } else {
        redis = require('redis').createClient();
        subscriber = require('redis').createClient();

        console.log('Localhost redis connection is inited');
    }
}

function redisStop() {
    if(redis || subscriber) {
        if(redis) {
            redis.quit();
        }

        if(subscriber) {
            subscriber.quit();
        }

        console.log('Redis connections are stopped');
    }
}